import java.util.Scanner;

public class Main {

    public static float findAvg(int size, int a[]) {
        float sum=0;
        for (int i=0; i<size; i++) {
            sum+=a[i];
        }
        return sum/size;
    }

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int size = s.nextInt();

        int a[]= new int[size];
        for(int i=0; i<size; i++) {
            a[i] = s.nextInt();
        }

        System.out.println(findAvg(size, a));
    }
}
